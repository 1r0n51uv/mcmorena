import React from 'reactn';

class Storia extends React.PureComponent {
    render() {
        return (
            <div>
                <section id="single-page-slider" className="no-margin" style={{backgroundImage: `url('https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F5977.jpg?alt=media&token=e6838ee7-14bd-49da-a8c1-6cb5b2a739f6')`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover'
                }}>
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="center gap fade-down section-heading">
                                                <h2 className="main-title"><i className="fas fa-medal"/> I nostri successi sportivi</h2>
                                                <hr/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="about-us" className="white">
                    <div className="container">
                        <div className="gap"/>



                        <div className="row">
                            <div className="col-md-10 col-md-offset-1 fade-up">
                                <p>Se per  tanti  moto  club  l'attività  agonistica  rappresenta  un  "grosso  peso"  da  evitare,
                                    per il Motoclub Morena è stato, fin dalla sua costituzione, uno degli impegni primari.
                                    Senza  l'agonismo  il  "giovane"  difficilmente  si  avvicina  ad  un  moto  club  e,
                                    secondo noi, è principalmente con l'attività agonistica che si vitalizza il motociclismo.
                                    Se   con   il   mototurismo   e   l'attività   sociale   il   Motoclub   Morena   non   è
                                    secondo   a nessuno in Italia, con l'attività agonistica è da sempre in prima fila.
                                    Subito  nel  1977  parte  alla  grande  nella  velocità  con  il  team  "dei  Brollo"
                                    di  Gemona, dei  piloti  <b>Saverio</b>  ed  <b>Eliseo</b>  e  del  tuttofare  <b>Aurelio</b>  arrivando  per
                                    diversi  anni  ad  un passo dal titolo italiano. Altri piloti arrivano al Motoclub Morena :
                                    <b>Walter Cussigbr</b>, <b>Mario Pittoni</b>, <b>Ferdinando De Cecco</b>, <b>Corrado Nardini </b>
                                    ed  il  gruppo  dei  "monfalconesi"  che  con  <b>Alan Benedetti</b> formeranno  in
                                    seguito  il "Team  Motovelocità  Friulana",  prima  attività  sportiva  ad  ottenere
                                    il  patrocinio  "Made in Friuli". Sempre  nella  velocità  nel  1977  nasce  anche  un
                                    team  "mondiale"  per  merito  del Tecno Sport Morena e del suo titolare, nonché socio
                                    fondatore del Motoclub
                                    <b>Giuliano Gemo</b>. Il  Team  era  formato  da  Rino  Zuliani  con  MBA  125,
                                dal  sudafricano  <b>Singer  Graham</b> con la Yamaha 250 e la Bimota 350 nonché da <b>Corrado Tuzzi</b> con la Suzuki 500 RG.
                                    Per   molti   anni   la   velocità   darà   non   poche   soddisfazioni   portando   il   nome
                                    del Motoclub  Morena  sulle  piste  di  tutto  il  mondo,  partecipando  a  campionati  italiani, europei  e
                                    mondiali  con  la  conquista  di  cinque  titoli  italiani  TT  con  <b>Walter  Cussig</b>  ed uno con <b>Ferdinando
                                        De Cecco</b> oltre ad un Trofeo Suzuki con <b>Alan Benedetti</b>.</p>

                                <p>La  storia  continua  con  <b>Michele  Conti</b>  Campione  Europeo  2005  e  nel  2006  impegnato nel   Campionato
                                    del   Mondo   Velocità   con   la   sua   Honda   125   GP,   senza   tralasciare <b>Samuela De Nardi</b>
                                    , Campionessa Europea 2005 nella categoria Velocità femminile.
                                    Ancora oggi i nostri piloti primeggiano nel settore Velocità con: <b>Andrea Di Vora</b> e <b>Roberto Anastasia</b>.
                                    Negli  anni  80  il  Motoclub  Morena  s'impegnò  per  qualche  tempo  anche  nello  Speedway con
                                    alcuni  piloti  locali  e  sempre  negli  anni  80  introdusse  in  Friuli  il  Trial  organizzando
                                    le prime gare con i primi piloti regionali. Ma  è  nel  fuoristrada  che  il  Motoclub  Morena  esplode
                                    nel  1979  con  l'apporto  del  Team Zamoto  e  del  suo  titolare  <b>Flavio  Zamò</b>.  Inizia  un'epoca
                                    "storica"  per  le  gare  ed  i  piloti del Friuli Venezia Giulia nell'Enduro e nel Motocross.
                                    Titoli  italiani  sono  stati  vinti  nell'Enduro  con  :  <b>Daniele  Gerussi</b>,  <b>Eros  Marcuzzi</b>,
                                    <b>Carlo Toso</b>, <b>Gianfranco  Crivellari</b>,  <b>Roberto  Massarotti</b>,  <b>Ferruccio  Degano</b>,
                                    <b>Mauro  Sant</b>.  Tanti titoli  italiani sono  stati  persi  per  un  soffio  e  tanti  sono  stati  i  piloti  prestigiosi  che  con  il Motoclub
                                    Morena hanno fatto la storia dell' Enduro.Numerose  sono  state  le  partecipazioni  alle  "Sei  giorni
                                    mondiale  Enduro"  con  tante medaglie d'oro vinte. Una particolare citazione spetta ad <b>Edi Orioli</b> uomo i
                                    mmagine delfuoristrada del Motoclub Morena, per tanto tempo numero uno mondiale.Il   Motoclub   Morena
                                    ha,   da   sempre,   organizzato   la   gara   Internazionale   "Enduro Lignano",   la   più   importante
                                    manifestazione   motoristica   italiana   ed   europea   sulla sabbia, specchio ideale delle potenziali
                                    capacità e dimensioni del Motoclub Morena. Nei  Rally  Marathon  un  titolo  Europeo  è  stato  vinto
                                    con  <b>Michele  Toros</b>  e  <b>Bruno  Galletti</b> ed inoltre titoli italiani sono stati vinti da <b>Silvano Nascig</b>,
                                    <b>Andrea Purinan</b>. Nel  Motocross  il  Motoclub  Morena  vanta  il  primo  pilota  friulano  vincitore  di  un
                                    titolo Triveneto  con  <b>Sandro  Pravisani</b>,  che  lasciò  il  testimone  a  <b>Giorgio  Battig</b>,  <b>Roberto Massarotti</b>,
                                    <b> Mino Della Morte</b>, <b>Flavio Marini</b> e <b>Luca Morettin</b>. Molto  impegno  il  Motoclub  Morena  ha  sempre  avuto  nel
                                    Minimotocross,  tanto  da essere   stato   il   Motoclub   fondatore   della   specialità   in   Italia   e
                                    di   essere   uno   dei motoclub   più   rappresentativi   con   <b>Alex   Battig</b>   Campione   italiano   2004,
                                    <b>Agostino Panzani</b> Campione Triveneto, tuttora impegnato nel Campionato Italianoe  negli  anni  appena  passati
                                    con  <b>Martino  Panzani</b>,  <b>Luca  Coppola</b>,  <b>Lorenzo  Macoritto</b> Campione  Italiano  Minienduro  2008  e  tanti  altri
                                    ragazzini  ora  brillantemente  avviati  alle specialità maggiori.Nel   concludere   questo   breve   cenno
                                    sull'attività   agonistica   del   Motoclub   Morena riteniamo giusto ricordare i Presidenti del Motoclub
                                    Morena: <b>Mario Del Negro</b>, <b>Giuliano Gemo</b>, <b>Dario Furlan</b>, <b>Claudio Boffitor</b>.
                                    Itenendo  nel  ricordare  loro  di  nominare  tutti  quelli  che  non  sono  stati  qui  scritti poiché  è
                                    impossibile  elencare  tutti  i  Campioni  ed  i  Campionati  Regionali  e  Triveneti vinti dal Motoclub Morena,
                                    basti dire che... <h3 className="text-center">il Motoclub Morena ha vinto, da solo, più di tutti i motoclub della regione messi insieme!</h3></p>
                            </div>
                            <div className="col-md-4 fade-up">

                            </div>
                        </div>


                        <div className="gap"/>

                    </div>
                </section>
                <section className="no-margin" style={{backgroundColor: '#202020'}}>
                    <div className="container">
                        <div className="row fade-up">
                            <div className="center gap fade-down section-heading">
                                <h2 style={{color: 'white'}} className="main-title">I nostri campioni</h2>
                                <hr/>
                            </div>

                            <div className="col-md-6">
                                <div className="testimonial-list-item">
                                    <img className="pull-left img-responsive quote-author-list"
                                         src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/history%2Fediorioli.jpg?alt=media&token=059cc272-a754-4591-93c8-95b392484ce6"
                                         alt=""
                                    />
                                    <blockquote>
                                        <h3><i className="fas fa-trophy"/> Edi Orioli</h3>
                                        <p>Uomo i mmagine del fuoristrada del Motoclub Morena, per tanto tempo numero uno mondiale</p>
                                    </blockquote>
                                </div>

                                <div className="testimonial-list-item">
                                    <img className="pull-left img-responsive quote-author-list"
                                         alt=""
                                         src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/history%2Fmicheleconti.jpg?alt=media&token=0c02fb71-7fbb-4340-9929-6c319ff144d7"/>
                                    <blockquote>
                                        <h3><i className="fas fa-trophy"/> Michele Conti</h3>
                                        <p>Campione Europeo 2005 e nel 2006 impegnato nel Campionato del Mondo Velocità con la sua Honda 125 GP.</p>
                                    </blockquote>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="testimonial-list-item">
                                    <img className="pull-left img-responsive quote-author-list"
                                         alt=""
                                         src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/history%2Fsamuela%20de%20nardi.jpg?alt=media&token=67a21140-2d22-4dc7-8fde-c0f69bd97d8f"/>
                                    <blockquote>
                                        <h3><i className="fas fa-trophy"/> Samuela De Nardi</h3>
                                        <p>Campionessa Europea 2005 nella categoria Velocità femminile</p>
                                    </blockquote>
                                </div>
                                <div className="testimonial-list-item">
                                    <img className="pull-left img-responsive quote-author-list"
                                         alt=""
                                         src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/history%2Fvalter.jpg?alt=media&token=12fa2359-51b2-4b69-b92f-982d3752970b"/>
                                    <blockquote>
                                        <h3><i className="fas fa-trophy"/> Walter Cussig</h3>
                                        <p>Conquista  di  cinque  titoli  italiani  TT</p>
                                    </blockquote>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
        );
    }
}

export default Storia;