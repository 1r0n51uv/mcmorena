import React from 'reactn';

class Notfound extends React.PureComponent {
    render() {
        return (

            <section id="contact" className="white">
                <div className="container">
                    <div className="gap"></div>
                    <div className="center gap fade-down section-heading">
                        <h1 className="main-title">Pagina non trovata!</h1>
                        <hr/>
                        <a href="home" className="btn btn-md btn-outlined btn-success">
                            <i style={{fontSize: '120%'}} className="fas fa-arrow-left"/> Home</a>
                    </div>
                </div>
            </section>

        );
    }
}

export default Notfound;