import React from 'reactn';
import HomeSlider from "../elements/home_slider";

class Home extends React.PureComponent {
    render() {
        return (
            <div>
                <HomeSlider/>


                <section id="testimonial-carousel" className="divider-section">
                    <div className="gap"/>

                    <div className="container">
                        <div className="row">
                            <div className="center gap fade-down section-heading" style={{paddingLeft: '5%', paddingRight: '5%'}}>
                                <div className="gap"/>
                            </div>
                        </div>
                    </div>


                </section>
            </div>
        );
    }
}

export default Home;