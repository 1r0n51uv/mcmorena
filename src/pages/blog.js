import React from 'reactn';
import Loader from "react-loader-spinner";
import {FirestoreCollection} from "react-firestore";


class Blog extends React.PureComponent {
    render() {
        return (
            <div>
                <section id="single-page-slider" className="no-margin" style={{backgroundImage: `url('https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F5977.jpg?alt=media&token=e6838ee7-14bd-49da-a8c1-6cb5b2a739f6')`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover'
                }}>
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="center gap fade-down section-heading">
                                                <h2 className="main-title"><i className="far fa-newspaper"/> Blog</h2>
                                                <hr/>
                                                <p>I nostri eventi, raduni ed incontri</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="blog" className="white">
                    <div className="container">
                        <hr/>
                        <FirestoreCollection
                            path="blog"
                            sort="date:desc"
                            render={({ isLoading, data }) => {
                                return isLoading ? (
                                    <div className="col-md-12 text-center">
                                        <Loader
                                            type="Puff"
                                            color="black"
                                            height="100"
                                            width="100"
                                        />
                                    </div>
                                ) : (
                                    <div className="container">

                                        <div className="row">

                                            {
                                                data.map(post => (
                                                    <div className="col-md-4 fade-in" key={post.id}>
                                                        <div className="post" style={{backgroundColor: '#1A1A1A', color: 'white'}}>
                                                            <div className="post-img-content" style={{backgroundColor: 'white'}}>
                                                                <img src={post.img} className="img-responsive center-block" style={{height: '150px', padding: '1%'}}/>
                                                            </div>
                                                            <div className="content">
                                                                <h2 className="post-title">{post.title}</h2>
                                                                <div className="author">
                                                                    <i className="fas fa-map-marker"/> <b>{post.where} </b> |
                                                                    <i className="fas fa-clock"/> <time dateTime="01-20-2014">  {post.date} </time>
                                                                </div>
                                                                <div className="read-more-wrapper">
                                                                    <a href={'/' + post.id } className="btn btn-outlined btn-primary">Leggi tutto</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))
                                            }

                                        </div>

                                    </div>
                                );
                            }}
                        />

                    </div>
                    <div className="gap"/>
                </section>
            </div>

        );
    }
}

export default Blog;