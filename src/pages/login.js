import React, { setGlobal } from 'reactn';
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class Login extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pass: ''
        };


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.login(this.state.pass);
    }

    login(pass) {
        if (pass === 'moto') {
            this.setGlobal({
                auth: true
            });
            this.props.history.push('/admin');
        } else {
            toast.error('Password Errata');
        }
    }

    render() {
        return (
            <div>

                <section id="single-page-slider" className="no-margin" style={{backgroundColor: '#7A7A7A'}}>
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="center gap fade-down section-heading">
                                                <h2 className="main-title">Admin</h2>
                                                <hr/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="contact" className="white">
                    <div className="container">

                        <div className="gap"></div>
                        <div className="row center">
                            <div className="col-md-offset-3 col-md-6 fade-up">
                                <h3>Password</h3>
                                <div id="message"></div>
                                <form id="contactform" onSubmit={this.handleSubmit} >
                                    <input type="password"
                                           name="pass"
                                           id="name"
                                           value={this.state.pass}
                                           onChange={this.handleChange}/>
                                    <input className="btn btn-outlined btn-primary" type="submit" name="submit"
                                           value="Accedi"/>
                                </form>
                            </div>
                        </div>

                    </div>
                </section>


                <ToastContainer/>
            </div>
        );
    }
}

export default Login;