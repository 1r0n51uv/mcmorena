import React from 'reactn';
import Mapcontainer from "../elements/mapcontainer";
import Contactcard from "../elements/contactcard";

class Contacts extends React.PureComponent {
    constructor(props) {
        super(props);

    }


    render() {
        return (
            <div>

                <section id="single-page-slider" className="no-margin" style={{backgroundImage: `url('https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F5977.jpg?alt=media&token=e6838ee7-14bd-49da-a8c1-6cb5b2a739f6')`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover'
                }}>
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="center gap fade-down section-heading">
                                                <h2 className="main-title"><i className="fas fa-mail-bulk"/>  Contatti</h2>
                                                <hr/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="container">
                    <div className="gap"/>

                    <div className="center gap fade-down section-heading">
                        <h2 className="main-title">Dove siamo</h2>
                        <hr/>
                    </div>
                </div>

                <Mapcontainer/>

                <div className="container">
                    <div className="gap"/>

                    <div className="center gap fade-down section-heading">
                        <div className="row">

                            <div className="col-md-offset-1 col-md-4 text-left" style={{borderLeft: '1px solid black'}}>
                                <h2>
                                    La nostra sede
                                </h2>
                                <a href="https://www.google.com/maps/place/Via+Brigata+Re,+29,+33100+Udine+UD/data=!4m2!3m1!1s0x477a4a964524f04d:0x7bdf56bfc9f7e86e?ved=2ahUKEwjEwZKf0pLgAhVvMewKHcvyCwoQ8gEwAHoECAAQAQ"><h4>Via Brigata Re 29,
                                    <br/>33100 Udine Italia <i style={{fontSize: '120%', color: '#4BA35D'}} className="fas fa-location-arrow"/></h4></a>


                            </div>

                            <div className="col-md-2">

                            </div>
                            <div className="col-md-4 text-left" style={{borderLeft: '1px solid black'}}>
                                <h2>Contatti</h2>
                                <a href="https://www.facebook.com/motoclubmorena.it/"><h4><i style={{fontSize: '160%', color: '#3b5998'}} className="fab fa-facebook-square"/> MotoClub Morena</h4></a>
                                <a href="https://www.instagram.com/motoclubmorena/"><h4><i style={{fontSize: '160%', color: '#e95950'}} className="fab fa-instagram"/> @motoclubmorena</h4></a>
                                <a href="#"><h4><i style={{fontSize: '140%'}} className="far fa-envelope"/> info@motoclubmorena.it</h4></a>
                                <a href="#"><h4><i style={{fontSize: '140%'}} className="far fa-envelope"/> mcmorenaudine@gmail.com</h4></a>
                                <a href="#"><h4><i style={{fontSize: '160%', color: '#25D366'}} className="fas fa-phone-square"/> 338 3975506</h4></a>

                            </div>

                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="gap"/>

                    <div className="center gap fade-down section-heading">
                        <h2 className="main-title">Direttivo</h2>
                        <hr/>
                    </div>

                    <div className="gap"/>

                    <div>
                        <div id="meet-the-team" className="row">

                            <Contactcard
                                name="Sergio Ammirati"
                                img="https://graph.facebook.com/1635289801/picture?type=large&width=720&height=720"
                                ruolo="Presidente"
                                email={ ["presidente", <br/>, "motoclubmorena.it"]}
                            />


                            <Contactcard
                                name="Bruno Bertoni"
                                img="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/direttivo%2Fbrunobertoni.jpg?alt=media&token=5051c8c7-b1ce-4bab-9f19-58f1206c53dd"
                                ruolo="Vice Presidente"
                                email={ ["vicepresidente", <br/>, "motoclubmorena.it"]}
                            />

                            <Contactcard
                                name="Antonio Cantarutti"
                                img="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/direttivo%2Fantoniocantrutti.jpg?alt=media&token=3ffe27de-891b-4dde-98b4-e505ef2d493e"
                                ruolo="Segretario"
                                email={ ["segretario", <br/>, "motoclubmorena.it"]}
                            />

                            <Contactcard
                                name="Roberto Conti"
                                img="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/direttivo%2Frobertoconti.jpg?alt=media&token=812e84c6-4e9c-4ec8-a48d-f70fdea698f2"
                                ruolo="Consigliere"
                                email={ ["roberto.conti", <br/>, "motoclubmorena.it"]}
                            />


                        </div>

                        <div id="meet-the-team" className="row">



                            <Contactcard
                                name="Vincenzo Frasca"
                                img="https://graph.facebook.com/599783684/picture?type=large&width=720&height=720"
                                ruolo="Consigliere"
                                email={ ["minimoto", <br/>, "motoclubmorena.it"]}
                            />

                            <Contactcard
                                name="Giovanni Piras"
                                img="https://graph.facebook.com/100004912306084/picture?type=large&width=720&height=720"
                                ruolo="Consigliere"
                                email={ ["giovanni.piras", <br/>, "motoclubmorena.it"]}
                            />


                        </div>
                    </div>
                </div>



            </div>
        );
    }
}

export default Contacts;