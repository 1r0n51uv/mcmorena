import React from 'reactn';
import firebase from 'firebase/app';
import { Scrollbars } from 'react-custom-scrollbars';
import Loader from "react-loader-spinner";
import {FirestoreCollection} from "react-firestore";




class Single extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            key: this.props.location.pathname.substring(1, this.props.location.pathname.length),
            title: "",
            date: "",
            where: "",
            description: "",
            img: ""

        };

    }

    componentDidMount() {
        firebase.firestore().collection('blog').doc(this.state.key).get().then(value => {
            this.setState({
                title: value.data()['title'],
                date: value.data()['date'],
                where: value.data()['where'],
                description: value.data()['description'],
                img: value.data()['img']
            });
        }).catch(err => {
            console.log(err);
        });
    }


    render() {
        return (
            <div>

                <div id="content-wrapper">
                    <section id="blog">
                        <div className="container">
                            <div className="row">


                                {
                                    this.state.title !== '' &&
                                    <div className="col-sm-8 col-sm-push-4" style={{color: 'white'}}>
                                        <div className="blog">
                                            <div className="blog-item" style={{backgroundColor: '#1A1A1A'}}>
                                                <div className="blog-featured-image">
                                                    <img className="img-responsive img-blog" alt="" src={this.state.img}/>
                                                </div>
                                                <div className="blog-content">
                                                    <h3 className="main-title">{this.state.title}</h3>
                                                    <div className="entry-meta">

                                                        <span><i className="fas fa-map-marker"/> {this.state.where}</span>
                                                        <span><i className="fas fa-clock"/> {this.state.date}</span>

                                                    </div>
                                                    <hr/>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                }

                                {
                                    this.state.title !== '' && <aside className="col-sm-4 col-sm-pull-8">
                                        <div className="center gap fade-down section-heading">
                                            <h3 className="main-title">Scorri tra le immagini</h3>
                                            <hr/>
                                        </div>
                                        <div className="tile-progress tile-cyan bounce-in" style={{backgroundColor: '#202020'}}>
                                            <Scrollbars style={{height: '1000px'}}>

                                                <FirestoreCollection
                                                    path={'blog/' + this.state.key + '/pix'}
                                                    render={({ isLoading, data }) => {
                                                        return isLoading ? (
                                                            <div className="col-md-12 text-center" style={{padding: '3%'}}>
                                                                <Loader
                                                                    type="Puff"
                                                                    color="white"
                                                                    height="100"
                                                                    width="100"
                                                                />
                                                            </div>
                                                        ) : (
                                                            <div>
                                                                {
                                                                    data.map(post => (
                                                                        <div style={{padding: '3%'}} key={post.id}>
                                                                            <a href={post.img}>
                                                                                <img src={post.img} className="img-responsive" alt=""/>
                                                                            </a>

                                                                        </div>
                                                                    ))
                                                                }



                                                            </div>
                                                        );
                                                    }}
                                                />

                                            </Scrollbars>
                                        </div>


                                    </aside>
                                }


                            </div>

                        </div>
                    </section>

                </div>

            </div>
        );
    }
}

export default Single;