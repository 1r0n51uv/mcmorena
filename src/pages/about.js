import React from 'reactn';

class About extends React.PureComponent {
    render() {
        return (

            <div>
                <section id="single-page-slider" className="no-margin" style={{backgroundImage: `url('https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F5977.jpg?alt=media&token=e6838ee7-14bd-49da-a8c1-6cb5b2a739f6')`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover'
                }}>
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="center gap fade-down section-heading">
                                                <h2 className="main-title"><i className="fas fa-motorcycle"/> Chi siamo?</h2>
                                                <hr/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="testimonial-carousel" className="divider-section">
                    <div className="gap"></div>

                    <div className="container">
                        <div className="row">
                            <div className="center gap fade-down section-heading" style={{paddingLeft: '5%', paddingRight: '5%'}}>
                                <p>La nostra è un'associazione sportivo-dilettantistica che ha lo scopo di promuovere l'attività agonistica dilettantistica,
                                    promuovere il mototurismo, far conoscere il territorio e le sue offerte attraverso motoraduni, motogiri e motoincontri.
                                    Siamo inoltre impegnati in attività benefiche e di assistenza ad eventi pubblici, il tutto condito dalla gran passione per
                                    le due ruote, la buona cucina, il buon vino, la birra, l'amicizia e la compagnia</p>
                                <div className="gap"></div>
                            </div>
                        </div>
                    </div>

                    <div className="container">
                        <div className="row">

                            <div className="col-md-4">
                                <div className="tile-progress tile-red bounce-in">
                                    <div className="tile-header">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F20980.jpg?alt=media&token=2f934aa6-44c9-4e75-ab39-cb0cd29dfd40"
                                             className="img-responsive"
                                             alt=""/>
                                    </div>
                                    <div className="tile-footer" style={{backgroundColor: '#1A1A1A'}}>
                                        <h4>
                                            La banda gira anche di notte..
                                        </h4>
                                    </div>

                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="tile-progress tile-red bounce-in">
                                    <div className="tile-header">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F20979.jpg?alt=media&token=5bf79b33-e13b-4ecd-bd72-cff023ebd13d"
                                             className="img-responsive"
                                             alt=""/>
                                    </div>
                                    <div className="tile-footer" style={{backgroundColor: '#1A1A1A'}}>
                                        <h4>
                                            Organizziamo motoraduni..
                                        </h4>
                                    </div>

                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="tile-progress tile-red bounce-in">
                                    <div className="tile-header">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F20981.jpg?alt=media&token=686dfa9a-b6dc-42bb-b30b-f849d329f9ac"
                                             className="img-responsive"
                                             alt=""/>
                                    </div>
                                    <div className="tile-footer" style={{backgroundColor: '#1A1A1A'}}>
                                        <h4>
                                            Organizziamo gite..
                                        </h4>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div className="gap"></div>
                    </div>

                    <div className="container">
                        <div className="row">

                            <div className="gap"></div>
                            <div id="pricing-table" className="row text-center">

                                <div className="col-md-4 col-sm-4 col-xs-12 flip-in">

                                    <ul className="plan plan1 featured">

                                        <li className="plan-name">
                                            <img src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F260577-P4K1BM-29-min.jpg?alt=media&token=249a05ea-9f94-4108-adfb-13be8a688cbe"
                                                 className="img-responsive" style={{paddingLeft: '5%', paddingRight: '5%', paddingTop: '5%'}} alt=""/>
                                        </li>

                                        <li style={{paddingLeft: '5%', paddingRight: '5%'}} className="plan-price">
                                            <p>Come   tutti   i   motociclisti   che   si rispettino   amiamo   viaggiare,   ma non  sempre  ci  piace  farlo  da  soli,   infatti      amiamo      viaggiare      in compagnia       scoprendo       nuovi panorami e ... nuovi locali. Il   nostro   gruppo   non   difetta   di componenti   che   sanno   scoprire nuove   strade,   nuove   trattorie   e nuovi  panorami  garantendo  così  la possibilità     di     organizzare     giri diversi         ad         ogni         uscita, programmata e non.</p>
                                        </li>
                                    </ul>
                                </div>

                                <div className="col-md-4 col-sm-4 col-xs-12 flip-in">

                                    <ul className="plan plan1 featured">

                                        <li className="plan-name">
                                            <img src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F814.jpg?alt=media&token=6a927988-75ec-4ce2-b361-c21d8a82b56d"
                                                 className="img-responsive" style={{paddingLeft: '5%', paddingRight: '5%', paddingTop: '5%'}} alt=""/>
                                        </li>

                                        <li style={{paddingLeft: '5%', paddingRight: '5%'}} className="plan-price">
                                            <p>Come  credo  tutti  sappiano,  il  Friuli è   una   terra   che   dispensa   ottime specialità     culinarie,     unitamente alla   Carnia,   alle   vicine   Slovenia, Austria    e    Croazia.    Permette    di sperimentare       realtà       culinarie diverse  tra  loro  che  spaziano  tra  le carni  e  le  verdure  per  arrivare  al pesce più pregiato. Una  menzione  particolare  per  i  vini rinomati    in    tutto    il    mondo.    Le località        del        Collio        sono sicuramente  le  più  conosciute  ma altre    contendono    la    fama    per specialità     e     qualità     del     vino prodotto.   I   nostri   vicini   invece, Karinzia  in  primis,  non  scherzano in fatto di birra.</p>
                                        </li>
                                    </ul>
                                </div>

                                <div className="col-md-4 col-sm-4 col-xs-12 flip-in">

                                    <ul className="plan plan1 featured">

                                        <li className="plan-name">
                                            <img src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/other%2F5b2a520f157132.62610391.jpg?alt=media&token=739e79ca-af5a-4fad-a7d2-63af2c046f0a"
                                                 className="img-responsive" style={{paddingLeft: '5%', paddingRight: '5%', paddingTop: '5%'}} alt=""/>
                                        </li>

                                        <li style={{paddingLeft: '5%', paddingRight: '5%'}} className="plan-price">
                                            <p>La  particolare  posizione  del  Friuli Venezia      Giulia      permette      di godere      di      paesaggi      molto variegati   e   tramonti   di   notevole diversità.   Infatti   le   località   più conosciute,   poste   agli   estremi della     regione,     sono     Lignano Sabbiadoro  e  Tarvisio,  tramonto riflesso  dal  mare  ed  immerso  tra  i monti  rispettivamente.  Al  centro di  questi  estremi  c’è  un  territorio fatto  di  pianura  e  colline  a  volte dolci  a  volte  aspre.  Non  abbiamo, né  avremo,  mai  finito  di  esplorare tutti      gli      splendidi      angolini nascosti.</p>
                                        </li>
                                    </ul>
                                </div>


                            </div>
                            <div className="gap"></div>


                        </div>
                        <div className="gap"></div>
                    </div>



                </section>
            </div>


        );
    }
}

export default About;