import React, {Component} from 'react';
import Slider from "react-slick";


class Carouselex extends Component {
    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return (
            <Slider {...settings}>
                <div>
                    <img className="img-responsive" src="https://firebasestorage.googleapis.com/v0/b/motomorena-4a906.appspot.com/o/slider%2Fauguri5.jpeg?alt=media&token=3a9a6a4f-98bd-4cfd-a483-2c907bdee9e2" alt=""/>
                </div>
                <div>
                    <h3>2</h3>
                </div>
                <div>
                    <h3>3</h3>
                </div>
                <div>
                    <h3>4</h3>
                </div>
                <div>
                    <h3>5</h3>
                </div>
                <div>
                    <h3>6</h3>
                </div>
            </Slider>
        );
    }
}

export default Carouselex;