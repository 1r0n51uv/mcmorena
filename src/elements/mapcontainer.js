import React from 'reactn';
import MapGL, {Marker } from '@urbica/react-map-gl'

const TOKEN2 = 'pk.eyJ1IjoiMXIwbjUxdXYiLCJhIjoiY2pvcG0ybmFkMGV3YTNwc3V4bGR3anJwOSJ9.t_p6LZuQcRQJa7r8Rka9-g';

class Mapcontainer extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                latitude: 46.0739636,
                longitude: 13.252308,
                zoom: 14,
            },
            marker: {
                latitude: 46.0739636,
                longitude: 13.252308
            },
            isOpen: true
        };
    }

    render() {


        const Element = <a onClick={() => {
            this.setState({isOpen: true
            })}
        } href="#"><i style={{fontSize: '400%', color: 'red'}} className="fas fa-map-marker-alt"/></a>

        return (
            <section id="portfolio" className="white">
                <div className="container">
                    <div className="row">

                        <div className="col-md-12">

                            <MapGL
                                style={{ width: "100%", height: "400px", marginTop: "2%" }}
                                mapStyle="mapbox://styles/mapbox/dark-v9"
                                accessToken={TOKEN2}
                                {...this.state.viewport}>
                                <Marker
                                    longitude={this.state.marker.longitude}
                                    latitude={this.state.marker.latitude}
                                    element={Element}
                                />
                            </MapGL>

                        </div>

                    </div>

                </div>
            </section>
        );
    }
}

export default Mapcontainer;