import React, {Component} from 'react';
import "./../mycarousel.css";
import { Carousel } from 'react-responsive-carousel';
import { FirestoreCollection } from "react-firestore";
import Loader from "react-loader-spinner";
import Modal from 'react-responsive-modal';


class HomeSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            galleryItems: [],
            open: false,
            selectedItem: 0
        }
    }


    onOpenModal = () => {
        this.setState({ open: true });
    };

    onCloseModal = () => {
        this.setState({ open: false });
    };

    render() {
        return (
            <div>

                <FirestoreCollection
                    path="slider"
                    render={({ isLoading, data }) => {
                        return isLoading ? (
                            <div className="col-md-12 text-center" style={{padding: '3%'}}>
                                <Loader
                                    type="Puff"
                                    color="black"
                                    height="100"
                                    width="100"
                                />
                            </div>
                        ) : (
                            <Carousel
                                showIndicators={false}
                                showStatus={false}
                                showThumbs={true}
                                autoPlay={true}
                                emulateTouch={true}
                                infiniteLoop={true}
                                dynamicHeight={true}
                                interval={7000}
                                selectedItem={this.state.selectedItem}
                                onChange={(e) => {
                                    this.setState({
                                        selectedItem: e,
                                    });
                                }}
                            >
                                {
                                    data.map(image => (
                                        <div key={image.id}>
                                            <img src={image.img} key={image.id} className="img-responsive" alt="Responsive image"/>
                                            <div className="legend" style={{backgroundColor: 'transparent'}}>
                                                <button onClick={this.onOpenModal} className="btn btn-success btn-xs">LEGGI</button>
                                            </div>
                                        </div>
                                    ))}
                            </Carousel>



                        );
                    }}
                />

                <Modal open={this.state.open} onClose={this.onCloseModal} center>
                    <h2>{this.state.selectedItem}</h2>
                </Modal>
            </div>




        );
    }
}

export default HomeSlider;