import React, {Component} from 'react';

class Navbar extends Component {
    render() {
        return (
            <header className="navbar navbar-inverse navbar-fixed-bottom" role="banner">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span className="sr-only">Toggle navigation</span>
                            <i className="fa fa-bars"/>
                        </button>
                        <a className="navbar-brand" href="/home"><h1 style={{fontSize: '60%'}}><span
                            style={{fontSize: '120%'}}
                            className="fas fa-motorcycle bounce-in"/> MotoClub Morena</h1></a>
                    </div>
                    <div className="collapse navbar-collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="/home">Home</a></li>
                            <li><a href="/storia">Storia</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/about">Chi siamo</a></li>
                            <li><a href="/contacts">Contatti</a></li>
                            
                        </ul>
                    </div>
                </div>

            </header>

        );
    }
}

export default Navbar;