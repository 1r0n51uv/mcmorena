import React, {Component} from 'react';

class Template extends Component {
    render() {
        return (
            <div>
                <header className="navbar navbar-inverse navbar-fixed-top " role="banner">
                    <div className="container">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                <span className="sr-only">Toggle navigation</span>
                                <i className="fa fa-bars"></i>
                            </button>
                            <a className="navbar-brand" href="index2.html"><h1><span
                                className="pe-7s-gleam bounce-in"></span> IMPACT</h1></a>
                        </div>
                        <div className="collapse navbar-collapse">
                            <ul className="nav navbar-nav navbar-right">
                                <li><a href="index2.html">Home</a></li>
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="services.html">Services</a></li>
                                <li><a href="portfolio.html">Portfolio</a></li>
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="contact-us.html">Contact</a></li>
                                <li className="dropdown active">
                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">Pages <i
                                        className="icon-angle-down"></i></a>
                                    <ul className="dropdown-menu">
                                        <li><a href="project-item.html">Project Single</a></li>
                                        <li><a href="blog-item.html">Blog Single</a></li>
                                        <li className="active"><a href="404.html">404</a></li>
                                    </ul>
                                </li>
                                <li><span className="search-trigger"><i className="fa fa-search"></i></span></li>
                            </ul>
                        </div>
                    </div>
                </header>

                <section id="main-slider" className="no-margin">
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <div className="carousel-content center centered">
                                                <span className="home-icon pe-7s-gleam bounce-in"></span>
                                                <h2 className="boxed animation animated-item-1 fade-down">WE GONNA HELP
                                                    YOU MAKE AN IMPACT</h2>
                                                <p className="boxed animation animated-item-2 fade-up">Our expertise
                                                    will guide you to success. Without Fail.</p>
                                                <br/>
                                                <a className="btn btn-md animation bounce-in" href="#services">Learn
                                                    More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div id="content-wrapper">
                    <section id="services" className="white">
                        <div className="container">
                            <div className="gap"></div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="center gap fade-down section-heading">
                                        <h2 className="main-title">Stuff We Do</h2>
                                        <hr/>
                                        <p>Of an or game gate west face shed. ﻿no great but music too old found
                                            arose.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-4 col-sm-6">
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="fa fa-camera fa fa-md"></i>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Photography</h3>
                                            <p>Nay middleton him admitting consulted and behaviour son household.
                                                Recurred advanced he oh together entrance speedily suitable. Ready tried
                                                gay state fat could boy its among shall.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-6">
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="fa fa-thumbs-o-up fa fa-md"></i>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Marketing</h3>
                                            <p>Unfeeling agreeable suffering it on smallness newspaper be. So come must
                                                time no as. Do on unpleasing possession as of unreserved.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-6">
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="fa fa-ticket fa fa-md"></i>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Event Management</h3>
                                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada
                                                fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="gap"></div>
                            <div className="row">
                                <div className="col-md-4 col-sm-6">
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="fa fa-star fa fa-md"></i>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Star Gazing</h3>
                                            <p>Yet joy exquisite put sometimes enjoyment perpetual now. Behind lovers
                                                eat having length horses vanity say had its</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-6">
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="fa fa-cogs fa fa-md"></i>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Software Support</h3>
                                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada
                                                fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-6">
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="fa fa-google-plus fa fa-md"></i>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">SEO Services</h3>
                                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada
                                                fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="gap"></div>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="center gap fade-down section-heading">
                                        <h2 className="main-title">Our Skills</h2>
                                        <hr/>
                                        <p>Of an or game gate west face shed. ﻿no great but music too old found
                                            arose.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="tile-progress tile-red bounce-in">
                                        <div className="tile-header">
                                            <h3>Video Editing</h3>
                                            <span>Our cutting room floor is messy.</span>
                                        </div>
                                        <div className="tile-progressbar">
                                        </div>
                                        <div className="tile-footer">
                                            <h4>
                                                <span className="pct-counter counter">65.5</span>%
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="tile-progress tile-cyan bounce-in">
                                        <div className="tile-header">
                                            <h3>Marketing</h3>
                                            <span>How well we can sell you and your brand.</span>
                                        </div>
                                        <div className="tile-progressbar">
                                        </div>
                                        <div className="tile-footer">
                                            <h4>
                                                <span className="pct-counter counter">98.5</span>%
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="tile-progress tile-primary bounce-in">
                                        <div className="tile-header">
                                            <h3>Web Development</h3>
                                            <span>We love servers and stuff.</span>
                                        </div>
                                        <div className="tile-progressbar">
                                        </div>
                                        <div className="tile-footer">
                                            <h4>
                                                <span className="pct-counter counter">90</span>%
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="tile-progress tile-pink bounce-in">
                                        <div className="tile-header">
                                            <h3>Coffee</h3>
                                            <span>We done make good joe, though.</span>
                                        </div>
                                        <div className="tile-progressbar">
                                        </div>
                                        <div className="tile-footer">
                                            <h4>
                                                <span className="pct-counter counter">10</span>%
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="gap"></div>
                        </div>
                    </section>


                    <section id="single-quote" className="divider-section">
                        <div className="container">
                            <div className="gap"></div>
                            <div className="row">
                                <div className='col-md-offset-2 col-md-8 fade-up'>
                                    <div className="carousel slide" data-ride="carousel" id="quote-carousel">
                                        <div className="carousel-inner">
                                            <div className="item active">
                                                <blockquote>
                                                    <div className="row">
                                                        <div className="col-sm-3 text-center">
                                                            <img className="img-responsive" src="images/team/team01.jpg"
                                                            />
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit
                                                                amet, consectetur, adipisci velit!</p>
                                                            <small>Someone famous</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="gap"></div>
                        </div>
                    </section>

                    <section id="about-us" className="white">
                        <div className="container">
                            <div className="gap"></div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="center gap fade-down section-heading">
                                        <h2 className="main-title">A Little About Us</h2>
                                        <hr/>
                                        <p>Of an or game gate west face shed. ﻿no great but music too old found
                                            arose.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-10 col-md-offset-1 fade-up">
                                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                        bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis
                                        sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                                        velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris
                                        vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora
                                        torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.
                                        Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque
                                        elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra,
                                        erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl
                                        quis neque. Suspendisse in orci enim.</p>

                                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis
                                        bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis
                                        sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum
                                        velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris
                                        vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora
                                        torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.
                                        Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque
                                        elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra,
                                        erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl
                                        quis neque. Suspendisse in orci enim.</p>
                                </div>
                                <div className="col-md-4 fade-up">

                                </div>
                            </div>
                            <div className="gap"></div>
                            <div className="row fade-up">
                                <div className="col-md-6">
                                    <div className="testimonial-list-item">
                                        <img className="pull-left img-responsive quote-author-list"
                                             src="images/team/team01.jpg"/>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
                                                posuere erat a ante.</p>
                                            <small>Manager at <cite title="Source Title">Company</cite></small>
                                        </blockquote>
                                    </div>
                                    <div className="testimonial-list-item">
                                        <img className="pull-left img-responsive quote-author-list"
                                             src="images/team/team01.jpg"/>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
                                                posuere erat a ante.</p>
                                            <small>Manager at <cite title="Source Title">Company</cite></small>
                                        </blockquote>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="testimonial-list-item">
                                        <img className="pull-left img-responsive quote-author-list"
                                             src="images/team/team01.jpg"/>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
                                                posuere erat a ante.</p>
                                            <small>Manager at <cite title="Source Title">Company</cite></small>
                                        </blockquote>
                                    </div>
                                    <div className="testimonial-list-item">
                                        <img className="pull-left img-responsive quote-author-list"
                                             src="images/team/team01.jpg"/>
                                        <blockquote>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
                                                posuere erat a ante.</p>
                                            <small>Manager at <cite title="Source Title">Company</cite></small>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                            <div className="gap"></div>
                            <div className="center gap fade-down section-heading">
                                <h2 className="main-title">Meet The Team</h2>
                                <hr/>
                                <p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
                            </div>
                            <div className="gap"></div>

                            <div id="meet-the-team" className="row">
                                <div className="col-md-3 col-xs-6">
                                    <div className="center team-member">
                                        <div className="team-image">
                                            <img className="img-responsive img-thumbnail bounce-in"
                                                 src="images/team/team01.jpg" alt=""/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary"
                                                   href="images/team/team01.jpg" rel="prettyPhoto"><i
                                                    className="fa fa-eye"></i></a>
                                            </div>
                                        </div>
                                        <div className="team-content fade-up">
                                            <h5>Daniel Jones
                                                <small className="role muted">Web Design</small>
                                            </h5>
                                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                            <a className="btn btn-social btn-facebook" href="#"><i
                                                className="fa fa-facebook"></i></a> <a
                                            className="btn btn-social btn-google-plus" href="#"><i
                                            className="fa fa-google-plus"></i></a> <a
                                            className="btn btn-social btn-twitter" href="#"><i
                                            className="fa fa-twitter"></i></a> <a
                                            className="btn btn-social btn-linkedin" href="#"><i
                                            className="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-3 col-xs-6">
                                    <div className="center team-member">
                                        <div className="team-image">
                                            <img className="img-responsive img-thumbnail bounce-in"
                                                 src="images/team/team02.jpg" alt=""/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary"
                                                   href="images/team/team02.jpg" rel="prettyPhoto"><i
                                                    className="fa fa-eye"></i></a>
                                            </div>
                                        </div>
                                        <div className="team-content fade-up">
                                            <h5>John Smith
                                                <small className="role muted">Marketing Director</small>
                                            </h5>
                                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                            <a className="btn btn-social btn-facebook" href="#"><i
                                                className="fa fa-facebook"></i></a> <a
                                            className="btn btn-social btn-google-plus" href="#"><i
                                            className="fa fa-google-plus"></i></a> <a
                                            className="btn btn-social btn-twitter" href="#"><i
                                            className="fa fa-twitter"></i></a> <a
                                            className="btn btn-social btn-linkedin" href="#"><i
                                            className="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6">
                                    <div className="center team-member">
                                        <div className="team-image">
                                            <img className="img-responsive img-thumbnail bounce-in"
                                                 src="images/team/team03.jpg" alt=""/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary"
                                                   href="images/team/team03.jpg" rel="prettyPhoto"><i
                                                    className="fa fa-eye"></i></a>
                                            </div>
                                        </div>
                                        <div className="team-content fade-up">
                                            <h5>Dave Gorman
                                                <small className="role muted">Web Design</small>
                                            </h5>
                                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                            <a className="btn btn-social btn-facebook" href="#"><i
                                                className="fa fa-facebook"></i></a> <a
                                            className="btn btn-social btn-google-plus" href="#"><i
                                            className="fa fa-google-plus"></i></a> <a
                                            className="btn btn-social btn-twitter" href="#"><i
                                            className="fa fa-twitter"></i></a> <a
                                            className="btn btn-social btn-linkedin" href="#"><i
                                            className="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6">
                                    <div className="center team-member">
                                        <div className="team-image">
                                            <img className="img-responsive img-thumbnail bounce-in"
                                                 src="images/team/team04.jpg" alt=""/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary"
                                                   href="images/team/team04.jpg" rel="prettyPhoto"><i
                                                    className="fa fa-eye"></i></a>
                                            </div>
                                        </div>
                                        <div className="team-content fade-up">
                                            <h5>Steve Smith
                                                <small className="role muted">Sales Assistant</small>
                                            </h5>
                                            <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                            <a className="btn btn-social btn-facebook" href="#"><i
                                                className="fa fa-facebook"></i></a> <a
                                            className="btn btn-social btn-google-plus" href="#"><i
                                            className="fa fa-google-plus"></i></a> <a
                                            className="btn btn-social btn-twitter" href="#"><i
                                            className="fa fa-twitter"></i></a> <a
                                            className="btn btn-social btn-linkedin" href="#"><i
                                            className="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="gap"></div>
                            <div className="gap"></div>
                        </div>
                    </section>

                    <section id="stats" className="divider-section">
                        <div className="gap"></div>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-3 col-xs-6">
                                    <div className="center bounce-in">
                                        <span className="stat-icon"><span
                                            className="pe-7s-timer bounce-in"></span></span>
                                        <h1><span className="counter">246000</span></h1>
                                        <h3>HOURS SAVED</h3>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6">
                                    <div className="center bounce-in">
                                        <span className="stat-icon"><span
                                            className="pe-7s-light bounce-in"></span></span>
                                        <h1><span className="counter">16875</span></h1>
                                        <h3>FRESH IDEAS</h3>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6">
                                    <div className="center bounce-in">
                                        <span className="stat-icon"><span
                                            className="pe-7s-graph1 bounce-in"></span></span>
                                        <h1><span className="counter">99999999</span></h1>
                                        <h3>HUGE PROFIT</h3>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6">
                                    <div className="center bounce-in">
                                        <span className="stat-icon"><span
                                            className="pe-7s-box2 bounce-in"></span></span>
                                        <h1><span className="counter">54875</span></h1>
                                        <h3>THINGS IN BOXES</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="gap"></div>
                    </section>

                    <section id="portfolio" className="white">
                        <div className="container">
                            <div className="gap"></div>
                            <div className="center gap fade-down section-heading">
                                <h2 className="main-title">Examples Of Excellence</h2>
                                <hr/>
                                <p>She evil face fine calm have now. Separate screened he outweigh of distance
                                    landlord.</p>
                            </div>
                            <ul className="portfolio-filter fade-down center">
                                <li><a className="btn btn-outlined btn-primary active" href="#" data-filter="*">All</a>
                                </li>
                                <li><a className="btn btn-outlined btn-primary" href="#" data-filter=".apps">Apps</a>
                                </li>
                                <li><a className="btn btn-outlined btn-primary" href="#"
                                       data-filter=".nature">Nature</a></li>
                                <li><a className="btn btn-outlined btn-primary" href="#"
                                       data-filter=".design">Design</a></li>
                            </ul>

                            <ul className="portfolio-items col-3 isotope fade-up">
                                <li className="portfolio-item apps isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio01.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio01.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item joomla nature isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio02.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio02.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item bootstrap design isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio03.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio03.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item joomla design apps isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio04.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio04.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item joomla apps isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio05.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio05.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item wordpress nature isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio06.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio06.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item joomla design apps isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio07.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio07.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item joomla nature isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio08.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio08.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <li className="portfolio-item wordpress design isotope-item">
                                    <div className="item-inner">
                                        <img src="images/portfolio/folio09.jpg" alt=""/>
                                        <h5>Portfolio Project</h5>
                                        <div className="overlay">
                                            <a className="preview btn btn-outlined btn-primary"
                                               href="images/portfolio/folio09.jpg" rel="prettyPhoto"><i
                                                className="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </section>

                    <section id="testimonial-carousel" className="divider-section">
                        <div className="gap"></div>
                        <div className="container">
                            <div className="row">
                                <div className="center gap fade-down section-heading">
                                    <h2 className="main-title">What They Have Been Saying</h2>
                                    <hr/>
                                    <p>Of an or game gate west face shed. ﻿no great but music too old found
                                        arose.</p>
                                    <div className="gap"></div>
                                </div>
                                <div className='col-md-offset-2 col-md-8 fade-up'>
                                    <div className="carousel slide" data-ride="carousel" id="quote-carousel">
                                        <ol className="carousel-indicators">
                                            <li data-target="#quote-carousel" data-slide-to="0" className="active"></li>
                                            <li data-target="#quote-carousel" data-slide-to="1"></li>
                                            <li data-target="#quote-carousel" data-slide-to="2"></li>
                                        </ol>
                                        <div className="carousel-inner">
                                            <div className="item active">
                                                <blockquote>
                                                    <div className="row">
                                                        <div className="col-sm-3 text-center">
                                                            <img className="img-responsive" src="images/team/team01.jpg"
                                                            />
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit
                                                                amet, consectetur, adipisci velit!</p>
                                                            <small>Someone famous</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div className="item">
                                                <blockquote>
                                                    <div className="row">
                                                        <div className="col-sm-3 text-center">
                                                            <img className="img-responsive" src="images/team/team02.jpg"
                                                            />
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                                Etiam auctor nec lacus ut tempor. Mauris.</p>
                                                            <small>Someone famous</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                            <div className="item">
                                                <blockquote>
                                                    <div className="row">
                                                        <div className="col-sm-3 text-center">
                                                            <img className="img-responsive" src="images/team/team03.jpg"
                                                            />
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                                Ut rutrum elit in arcu blandit, eget pretium nisl
                                                                accumsan. Sed ultricies commodo tortor, eu pretium
                                                                mauris.</p>
                                                            <small>Someone famous</small>
                                                        </div>
                                                    </div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="gap"></div>
                        </div>
                    </section>

                    <section id="blog" className="white">
                        <div className="container">
                            <div className="center gap fade-down section-heading">
                                <div className="gap"></div>
                                <h2 className="main-title">From The Blog</h2>
                                <hr/>
                                <p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
                            </div>
                            <div className="gap"></div>
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="post">
                                        <div className="post-img-content">
                                            <img src="images/portfolio/folio02.jpg" className="img-responsive"/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary" href="#"><i
                                                    className="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                        <div className="content">
                                            <h2 className="post-title">Post Title</h2>
                                            <div className="author">
                                                <i className="fa fa-user"></i> By <b>Author</b> | <i
                                                className="fa fa-clock-o"></i>
                                                <time dateTime="2014-01-20">April 11th, 2014</time>
                                            </div>
                                            <div>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem
                                                Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an
                                                unknown printer took a galley of type and scrambled it to make a type
                                                specimen book.
                                            </div>
                                            <div className="read-more-wrapper">
                                                <a href="#" className="btn btn-outlined btn-primary">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="post">
                                        <div className="post-img-content">
                                            <img src="images/portfolio/folio06.jpg" className="img-responsive"/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary" href="#"><i
                                                    className="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                        <div className="content">
                                            <h2 className="post-title">Post Title</h2>
                                            <div className="author">
                                                <i className="fa fa-user"></i> By <b>Author</b> | <i
                                                className="fa fa-clock-o"></i>
                                                <time dateTime="2014-01-20">April 11th, 2014</time>
                                            </div>
                                            <div>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem
                                                Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an
                                                unknown printer took a galley of type and scrambled it to make a type
                                                specimen book.
                                            </div>
                                            <div className="read-more-wrapper">
                                                <a href="#" className="btn btn-outlined btn-primary">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="post">
                                        <div className="post-img-content">
                                            <img src="images/portfolio/folio11.jpg" className="img-responsive"/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary" href="#"><i
                                                    className="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                        <div className="content">
                                            <h2 className="post-title">Post Title</h2>
                                            <div className="author">
                                                <i className="fa fa-user"></i> By <b>Author</b> | <i
                                                className="fa fa-clock-o"></i>
                                                <time dateTime="2014-01-20">April 11th, 2014</time>
                                            </div>
                                            <div>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem
                                                Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an
                                                unknown printer took a galley of type and scrambled it to make a type
                                                specimen book.
                                            </div>
                                            <div className="read-more-wrapper">
                                                <a href="#" className="btn btn-outlined btn-primary">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="gap"></div>
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="post">
                                        <div className="post-img-content">
                                            <img src="images/portfolio/folio02.jpg" className="img-responsive"/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary" href="#"><i
                                                    className="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                        <div className="content">
                                            <h2 className="post-title">Post Title</h2>
                                            <div className="author">
                                                <i className="fa fa-user"></i> By <b>Author</b> | <i
                                                className="fa fa-clock-o"></i>
                                                <time dateTime="2014-01-20">April 11th, 2014</time>
                                            </div>
                                            <div>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem
                                                Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an
                                                unknown printer took a galley of type and scrambled it to make a type
                                                specimen book.
                                            </div>
                                            <div className="read-more-wrapper">
                                                <a href="#" className="btn btn-outlined btn-primary">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="post">
                                        <div className="post-img-content">
                                            <img src="images/portfolio/folio06.jpg" className="img-responsive"/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary" href="#"><i
                                                    className="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                        <div className="content">
                                            <h2 className="post-title">Post Title</h2>
                                            <div className="author">
                                                <i className="fa fa-user"></i> By <b>Author</b> | <i
                                                className="fa fa-clock-o"></i>
                                                <time dateTime="2014-01-20">April 11th, 2014</time>
                                            </div>
                                            <div>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem
                                                Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an
                                                unknown printer took a galley of type and scrambled it to make a type
                                                specimen book.
                                            </div>
                                            <div className="read-more-wrapper">
                                                <a href="#" className="btn btn-outlined btn-primary">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="post">
                                        <div className="post-img-content">
                                            <img src="images/portfolio/folio11.jpg" className="img-responsive"/>
                                            <div className="overlay">
                                                <a className="preview btn btn-outlined btn-primary" href="#"><i
                                                    className="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                        <div className="content">
                                            <h2 className="post-title">Post Title</h2>
                                            <div className="author">
                                                <i className="fa fa-user"></i> By <b>Author</b> | <i
                                                className="fa fa-clock-o"></i>
                                                <time dateTime="2014-01-20">April 11th, 2014</time>
                                            </div>
                                            <div>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem
                                                Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an
                                                unknown printer took a galley of type and scrambled it to make a type
                                                specimen book.
                                            </div>
                                            <div className="read-more-wrapper">
                                                <a href="#" className="btn btn-outlined btn-primary">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="pricing" className="white">
                        <div className="container">
                            <div className="center gap fade-down section-heading">
                                <h2 className="main-title">So, How Much?</h2>
                                <hr/>
                                <p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
                            </div>
                            <div className="gap"></div>
                            <div id="pricing-table" className="row">
                                <div className="col-md-3 col-xs-6 flip-in">
                                    <ul className="plan plan1">
                                        <li className="plan-name">
                                            <h3>Basic</h3>
                                        </li>
                                        <li className="plan-price">
                                            <div>
                                                <span className="price"><sup>$</sup>10</span>
                                                <small>month</small>
                                            </div>
                                        </li>
                                        <li>
                                            <strong>5GB</strong> Storage
                                        </li>
                                        <li>
                                            <strong>1GB</strong> RAM
                                        </li>
                                        <li>
                                            <strong>400GB</strong> Bandwidth
                                        </li>
                                        <li>
                                            <strong>10</strong> Email Address
                                        </li>
                                        <li>
                                            <strong>Forum</strong> Support
                                        </li>
                                        <li className="plan-action">
                                            <a href="#"
                                               className="btn btn-outlined btn-primary btn-md btn-white">Signup</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-md-3 col-xs-6 flip-in">
                                    <ul className="plan plan2 featured">
                                        <li className="plan-name">
                                            <h3>Standard</h3>
                                        </li>
                                        <li className="plan-price">
                                            <div>
                                                <span className="price"><sup>$</sup>20</span>
                                                <small>month</small>
                                            </div>
                                        </li>
                                        <li>
                                            <strong>5GB</strong> Storage
                                        </li>
                                        <li>
                                            <strong>1GB</strong> RAM
                                        </li>
                                        <li>
                                            <strong>400GB</strong> Bandwidth
                                        </li>
                                        <li>
                                            <strong>10</strong> Email Address
                                        </li>
                                        <li>
                                            <strong>Forum</strong> Support
                                        </li>
                                        <li className="plan-action">
                                            <a href="#" className="btn btn-outlined btn-primary btn-md">Signup</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-md-3 col-xs-6 flip-in">
                                    <ul className="plan plan3">
                                        <li className="plan-name">
                                            <h3>Advanced</h3>
                                        </li>
                                        <li className="plan-price">
                                            <div>
                                                <span className="price"><sup>$</sup>40</span>
                                                <small>month</small>
                                            </div>
                                        </li>
                                        <li>
                                            <strong>50GB</strong> Storage
                                        </li>
                                        <li>
                                            <strong>8GB</strong> RAM
                                        </li>
                                        <li>
                                            <strong>1024GB</strong> Bandwidth
                                        </li>
                                        <li>
                                            <strong>Unlimited</strong> Email Address
                                        </li>
                                        <li>
                                            <strong>Forum</strong> Support
                                        </li>
                                        <li className="plan-action">
                                            <a href="#"
                                               className="btn btn-outlined btn-primary btn-md btn-white">Signup</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-md-3 col-xs-6 flip-in">
                                    <ul className="plan plan4">
                                        <li className="plan-name">
                                            <h3>Mighty</h3>
                                        </li>
                                        <li className="plan-price">
                                            <div>
                                                <span className="price"><sup>$</sup>100</span>
                                                <small>month</small>
                                            </div>
                                        </li>
                                        <li>
                                            <strong>50GB</strong> Storage
                                        </li>
                                        <li>
                                            <strong>8GB</strong> RAM
                                        </li>
                                        <li>
                                            <strong>1024GB</strong> Bandwidth
                                        </li>
                                        <li>
                                            <strong>Unlimited</strong> Email Address
                                        </li>
                                        <li>
                                            <strong>Forum</strong> Support
                                        </li>
                                        <li className="plan-action">
                                            <a href="#"
                                               className="btn btn-outlined btn-primary btn-md btn-white">Signup</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="gap"></div>
                        </div>
                    </section>

                    <div id="mapwrapper">
                        <div id="map"></div>
                    </div>

                    <section id="contact" className="white">
                        <div className="container">
                            <div className="gap"></div>
                            <div className="center gap fade-down section-heading">
                                <h2 className="main-title">Get In Touch</h2>
                                <hr/>
                                <p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
                            </div>
                            <div className="gap"></div>
                            <div className="row">
                                <div className="col-md-4 fade-up">
                                    <h3>Contact Information</h3>
                                    <p><span className="icon icon-home"></span>Time Square, New York<br/>
                                        <span className="icon icon-phone"></span>+36 65984 405<br/>
                                        <span className="icon icon-mobile"></span>+36 65984 405<br/>
                                        <span className="icon icon-envelop"></span> <a
                                            href="#">email@infinityteam.com</a> <br/>
                                        <span className="icon icon-twitter"></span> <a href="#">@infinityteam.com</a>
                                        <br/>
                                        <span className="icon icon-facebook"></span> <a href="#">Infinity Agency</a>
                                        <br/>
                                    </p>
                                </div>

                                <div className="col-md-8 fade-up">
                                    <h3>Drop Us A Message</h3>
                                    <br/>
                                    <br/>
                                    <div id="message"></div>
                                    <form method="post" action="sendemail.php" id="contactform">
                                        <input type="text" name="name" id="name" placeholder="Name"/>
                                        <input type="text" name="email" id="email" placeholder="Email"/>
                                        <input type="text" name="website" id="website" placeholder="Website"/>
                                        <textarea name="comments" id="comments"
                                                  placeholder="Comments"/>
                                        <input className="btn btn-outlined btn-primary" type="submit"
                                               name="submit" value="Submit"/>
                                    </form>
                                </div>
                            </div>
                            <div className="gap"></div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

export default Template;