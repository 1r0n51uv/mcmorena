import React, {Component} from 'react';

class Contactcard extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="col-md-3 col-xs-6">
                <div className="center team-member">
                    <div className="team-image">
                        <img className="img-responsive img-thumbnail bounce-in"
                             src={this.props.img} alt=""/>

                    </div>
                    <div className="team-content fade-up">
                        <h5>{this.props.name}
                            <small className="role muted">{this.props.ruolo}</small>
                        </h5>
                        <a href="#"><i className="far fa-envelope-open"/> {this.props.email}</a>

                    </div>
                </div>
            </div>

        );
    }
}

export default Contactcard;