import React from 'reactn';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "../pages/home";
import Storia from "../pages/storia";
import Blog from "../pages/blog";
import Contacts from "../pages/contacts";
import AdminHome from "../admin/adminHome";
import Login from "../pages/login";
import ManageSlider from "../admin/manageSlider";
import InsertPost from "../admin/insertPost";
import Single from "../pages/single";
import ManageGallery from "../admin/manageGallery";
import About from "../pages/about";

class MyRouter extends React.PureComponent {


    render() {

        return (
            <Router>
                <div>

                    <Route path="/" exact component={Home} />
                    <Route path="/home" component={Home} />
                    <Route path="/storia" component={Storia} />
                    <Route path="/about" component={About} />
                    <Route path="/blog" component={Blog} />
                    <Route path="/contacts" component={Contacts} />
                    <Route path="/login" component={Login} />
                    <Route path="/admin" component={AdminHome} />
                    <Route path="/manageSlider" component={ManageSlider} />
                    <Route path="/insertPost" component={InsertPost} />
                    <Route path="/:id" component={Single}/>
                    <Route path="/insertGallery" component={ManageGallery}/>

                </div>

            </Router>
        );
    }
}

export default MyRouter;