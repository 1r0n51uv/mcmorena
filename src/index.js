import React, { setGlobal } from 'reactn';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import firebase from 'firebase/app';
import 'firebase/firestore';
import registerServiceWorker from './registerServiceWorker';
import { FirestoreProvider } from 'react-firestore';

const root = document.getElementById('root');

console.log(VARIABLE_TEST);

setGlobal({
    auth: false
});


const config = {
    apiKey: "AIzaSyDIwwGZLqmZe0oI98q6rATVh9n01Vl7ink",
    authDomain: "motomorena-4a906.firebaseapp.com",
    databaseURL: "https://motomorena-4a906.firebaseio.com",
    projectId: "motomorena-4a906",
    storageBucket: "motomorena-4a906.appspot.com",
    messagingSenderId: "539846898981"
};


firebase.initializeApp(config);
firebase.firestore().settings({timestampsInSnapshots: true});

ReactDOM.render(<FirestoreProvider firebase={firebase}> <App active={true}/> </FirestoreProvider>, root);
registerServiceWorker();


