import React, { Component } from 'react';
import './App.css';
import NavbarL from "./elements/navbar";
import MyRouter from "./elements/_router";
import Temp from "./pages/temp";



class App extends Component {
    render() {
        return (
            <div>
                { this.props.active &&
                <div>
                    <NavbarL/>
                    <MyRouter/>
                </div>}

                {!this.props.active && <Temp/>}


            </div>


        );
    }
}

export default App;
