import React from 'reactn';
import ManageSlider from "./manageSlider";
import InsertPost from "./insertPost";
import ManageGallery from "./manageGallery";

class AdminHome extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pageview: ''
        }
    }

    componentDidMount() {
        if (this.global.auth === false) {
            window.location.replace('/login')
        }
    }

    render() {
        return (
            <div>
                <section id="single-page-slider" className="no-margin" style={{backgroundColor: '#7A7A7A'}}>
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="center gap fade-down section-heading">
                                                <h2 className="main-title"><i className="fas fa-cog"/> Pannello Amministratore</h2>
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="services" className="white">
                    <div className="container">
                        <div className="gap"></div>

                        <div className="row">

                            <div className="col-md-4 col-sm-6">
                                <a href="#" onClick={() => (
                                    this.setState({
                                        pageview: 'editSlider'
                                    })
                                )}>
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="fas fa-images fa fa-md"/>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Modifica Slider</h3>
                                            <p>Gestisci le immagini dello slider nella Home</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-md-4 col-sm-6">
                                <a href="#" onClick={() => (
                                    this.setState({
                                        pageview: 'insertPost'
                                    })
                                )}>
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="far fa-calendar-plus fa fa-md"/>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Aggiungi un post</h3>
                                            <p>Aggiungi un post/evento/raduno nel Blog</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-md-4 col-sm-6">
                                <a href="#" onClick={() => (
                                    this.setState({
                                        pageview: 'editPost'
                                    })
                                )}>
                                    <div className="service-block">
                                        <div className="pull-left bounce-in">
                                            <i className="far fa-file-image fa fa-md"/>
                                        </div>
                                        <div className="media-body fade-up">
                                            <h3 className="media-heading">Aggiungi immagini ad un post</h3>
                                            <p>Aggiungi/Rimuovi immagini da un post nel Blog</p>
                                        </div>
                                    </div>
                                </a>
                            </div>




                        </div>

                        <div className="gap"></div>


                    </div>


                </section>


                { this.state.pageview === 'editSlider' && <ManageSlider/>}

                { this.state.pageview === 'insertPost' && <InsertPost/>}

                { this.state.pageview === 'editPost' && <ManageGallery/>}



            </div>


        );
    }
}

export default AdminHome;