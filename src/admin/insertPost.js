import React from 'reactn';
import firebase from "firebase/app";
import CustomUploadButton from 'react-firebase-file-uploader/lib/CustomUploadButton';
import { Line } from 'rc-progress';
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class InsertPost extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            filename: "",
            title: "",
            date: "",
            where: "",
            description: "",
            color: "black"
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }



    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.insertPost();
    }


    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    handleProgress = progress => this.setState({ progress });

    handleUploadError = error => {
        this.setState({ isUploading: false });
    };

    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref('blog/' + this.state.title)
            .child(filename)
            .getDownloadURL()
            .then(url => {
                this.setState({avatarURL: url, filename: filename});
            });

    };


    insertPost() {
        firebase.firestore().collection('blog').add({
            title: this.state.title,
            date: this.state.date,
            where: this.state.where,
            description: this.state.description,
            img: this.state.avatarURL
        }).then(() => {
            toast.success("Post caricato con successo");
            this.setState({
                avatar: "",
                isUploading: false,
                progress: 0,
                avatarURL: "",
                filename: "",
                title: "",
                date: "",
                where: "",
                description: ""
            });
        });

    }

        render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="center gap fade-down section-heading">
                                <h2 className="main-title">Inserisci un post</h2>
                                <hr/>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="gap"/>

                <div className="container">

                    <div className="row">

                        <div className="col-md-4">

                            <div className="post" style={{backgroundColor: '#1A1A1A', color: 'white'}}>
                                <div className="post-img-content" style={{backgroundColor: 'white'}}>
                                    <img src={this.state.avatarURL} alt="" className="img-responsive"/>
                                </div>
                                <div className="content">
                                    <h2 className="post-title">{this.state.title}</h2>
                                    <div className="author">
                                        <i className="fas fa-map-marker"/> <b>{this.state.where} </b> |
                                        <i className="fas fa-clock"/> <time dateTime="01-20-2014">{this.state.date} </time>
                                    </div>
                                    <div>
                                        {this.state.description.substring(0, 200)}...
                                    </div>
                                    <div className="read-more-wrapper">
                                        <a href="#" className="btn btn-outlined btn-primary">Leggi tutto</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="col-md-8 fade-up">

                            <div id="message"></div>
                            <form onSubmit={this.handleSubmit} id="contactform">
                                <input type="text"
                                       name="title"
                                       id="name"
                                       placeholder="Titolo.."
                                       onChange={this.handleChange}
                                       value={this.state.title}
                                />
                                <input type="date"
                                       name="date"
                                       id="name"
                                       placeholder="Data.."
                                       onChange={this.handleChange}
                                       value={this.state.date}
                                />
                                <input type="text"
                                       name="where"
                                       id="name"
                                       placeholder="Luogo.."
                                       onChange={this.handleChange}
                                       value={this.state.where}
                                />
                                <textarea name="description"
                                          id="comments"
                                          placeholder="Descrizione"
                                          onChange={this.handleChange}
                                          value={this.state.description}
                                />

                                <div className="tile-progress tile-cyan bounce-in">

                                    <div className="center">

                                        <CustomUploadButton
                                            accept="image/*"
                                            name="avatar"
                                            storageRef={firebase.storage().ref('blog/' + this.state.title)}
                                            onUploadStart={this.handleUploadStart}
                                            onUploadError={this.handleUploadError}
                                            onUploadSuccess={this.handleUploadSuccess}
                                            onProgress={this.handleProgress}
                                            style={{backgroundColor: 'green', color: 'white', padding: 10, borderRadius: 4, marginTop: '2%', marginBottom: '2%'}}
                                        >
                                            Carica Foto
                                        </CustomUploadButton>

                                        {this.state.isUploading &&
                                        <div>
                                            <Line percent={this.state.progress} strokeWidth="1" strokeColor={this.state.color}/>
                                            <div className="tile-footer">
                                                <h4>
                                                    <span className="pct-counter counter">{this.state.progress}</span>%
                                                </h4>
                                            </div>
                                        </div>

                                        }
                                    </div>


                                </div>
                                <div className="center">
                                    <input className="btn btn-outlined btn-warning" type="submit" name="submit"
                                           value="Inserisci"/>
                                </div>

                            </form>


                        </div>



                    </div>


                </div>

                <div className="gap"/>
                <div className="gap"/>
                <div className="gap" style={{marginBottom: '10%'}}/>

                <ToastContainer/>
            </div>
        );
    }
}

export default InsertPost;