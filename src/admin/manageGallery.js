import React from 'reactn';
import Loader from "react-loader-spinner";
import {FirestoreCollection} from "react-firestore";
import CustomUploadButton from "react-firebase-file-uploader/lib/CustomUploadButton";
import firebase from "firebase";
import Line from "rc-progress/es/Line";
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class ManageGallery extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            setview: 0,
            key: "",
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            filename: "",
            title: ""
        }
    }

    changeToAdd(chiave, titolo) {
        this.setState({
            setview: 1,
            key: chiave,
            title: titolo
        });

    }

    changeToEdit(chiave, titolo) {
        this.setState({
            setview: 2,
            key: chiave,
            title: titolo
        });

    }

    changeToChoose() {
        this.setState({
            setview: 0,
            key: ""
        })
    }

    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    handleProgress = progress => this.setState({ progress });

    handleUploadError = error => {
        this.setState({ isUploading: false });
    };

    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref('blog/' + this.state.title)
            .child(filename)
            .getDownloadURL()
            .then(url => {
                this.setState({avatarURL: url, filename: filename});
                this.insertGallery();
            }).then(() => {
                toast.success("Immagini inserite con successo");
        });
    };

    insertGallery() {
        firebase.firestore().collection('blog').doc(this.state.key).collection('pix').add({
            img: this.state.avatarURL,
            filename: this.state.filename
        });
    }

    removePic(key, filename) {
        firebase.firestore().collection('blog').doc(this.state.key).collection('pix').doc(key).delete().then(() => {
            firebase.storage().ref('blog/' + this.state.title).child(filename).delete().then(() => {
                toast.error("Immagine eliminata");
            }).catch(err => {
                console.log(err);
            }).catch(err2 => {
                console.log(err2);
            })
        })
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="center gap fade-down section-heading">
                        <h2 className="main-title">Scegli l'evento e crea una galleria</h2>
                        <hr/>
                    </div>
                </div>

                {
                    this.state.setview === 0 &&

                        <FirestoreCollection
                            path="blog"
                            sort="date:desc"
                            render={({ isLoading, data }) => {
                                return isLoading ? (
                                    <div className="col-md-12 text-center">
                                        <Loader
                                            type="Puff"
                                            color="black"
                                            height="100"
                                            width="100"
                                        />
                                    </div>
                                ) : (
                                    <div className="container">

                                        <div className="row">

                                            {
                                                data.map(post => (
                                                    <div className="col-md-4 fade-in" key={post.id} >
                                                        <div className="post" style={{backgroundColor: '#1A1A1A', color: 'white'}}>
                                                            <div className="post-img-content">
                                                                <img src={post.img} className="img-responsive center-block" style={{height: '150px'}}/>

                                                            </div>
                                                            <div className="content center">
                                                                <h2 className="post-title">{post.title}</h2>
                                                                <div className="gap"/>
                                                                <div className="center">
                                                                    <a href="#" onClick={() => {
                                                                        this.changeToAdd(post.id, post.title)
                                                                    }} className="btn btn-md btn-outlined btn-success">
                                                                        <i style={{fontSize: '120%'}} className="fas fa-plus"/> Aggiungi
                                                                    </a>
                                                                    <hr/>
                                                                    <a href="#" onClick={() => {
                                                                        this.changeToEdit(post.id, post.title)
                                                                    }} className="btn btn-md btn-outlined btn-info">
                                                                        <i style={{fontSize: '120%'}} className="far fa-edit"/> Modifica
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))
                                            }

                                        </div>

                                    </div>
                                );
                            }}
                        />

                }

                {
                    this.state.setview === 1 &&
                    <div>
                        <div className="container">
                            <a href="#" onClick={() => {
                                this.changeToChoose();
                            }} className="btn btn-md btn-outlined btn-info">
                                <i style={{fontSize: '120%'}} className="fas fa-arrow-left"/> Indietro</a>
                        </div>

                        <div className="gap"/>



                        <div className="tile-progress tile-cyan bounce-in container">

                            <div className="center">

                                <CustomUploadButton
                                    accept="image/*"
                                    name="avatar"
                                    storageRef={firebase.storage().ref('blog/' + this.state.title)}
                                    onUploadStart={this.handleUploadStart}
                                    onUploadError={this.handleUploadError}
                                    onUploadSuccess={this.handleUploadSuccess}
                                    onProgress={this.handleProgress}
                                    multiple={true}
                                    style={{backgroundColor: 'green', color: 'white', padding: 10, borderRadius: 4, marginTop: '2%', marginBottom: '2%'}}
                                >
                                    Carica Foto
                                </CustomUploadButton>

                                {this.state.isUploading &&
                                <div>
                                    <Line percent={this.state.progress} strokeWidth="1" strokeColor={this.state.color}/>
                                    <div className="tile-footer">
                                        <h4>
                                            <span className="pct-counter counter">{this.state.progress}</span>%
                                        </h4>
                                    </div>
                                </div>

                                }
                            </div>


                        </div>
                    </div>
                }

                {
                    this.state.setview === 2 &&
                        <div>

                            <div className="container">
                                <a href="#" onClick={() => {
                                    this.changeToChoose();
                                }} className="btn btn-md btn-outlined btn-info">
                                    <i style={{fontSize: '120%'}} className="fas fa-arrow-left"/> Indietro</a>
                            </div>

                            <div className="gap"/>
                            <FirestoreCollection
                                path={'blog/' + this.state.key + '/pix'}
                                render={({ isLoading, data }) => {
                                    return isLoading ? (
                                        <div className="col-md-12 text-center">
                                            <Loader
                                                type="Puff"
                                                color="black"
                                                height="100"
                                                width="100"
                                            />
                                        </div>
                                    ) : (
                                        <div className="container">

                                            <div className="row">

                                                {
                                                    data.map(image => (
                                                        <div className="col-md-4 fade-in center" key={image.id} >
                                                            <div className="post-img-content">
                                                                <img src={image.img} className="img-responsive"/>
                                                                <a href="#" onClick={() => {
                                                                    this.removePic(image.id, image.filename)
                                                                }} className="btn btn-md btn-outlined btn-danger">Rimuovi</a>
                                                            </div>

                                                        </div>
                                                    ))
                                                }

                                            </div>

                                        </div>
                                    );
                                }}
                            />
                        </div>


                }
                <ToastContainer/>
                <div className="gap"/>
                <div className="gap"/>
                <div className="gap"/>
                <div className="gap"/>
                <div className="gap"/>
            </div>
        );
    }
}

export default ManageGallery;