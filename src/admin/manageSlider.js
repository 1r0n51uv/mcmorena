import React from 'reactn';
import 'firebase/firestore'
import 'firebase/storage'
import { FirestoreCollection } from "react-firestore";
import firebase from "firebase"
import CustomUploadButton from 'react-firebase-file-uploader/lib/CustomUploadButton';
import { Line } from 'rc-progress';
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Loader from "react-loader-spinner";

class ManageSlider extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            filename: "",
            color: 'black'
        };
    }

    componentDidMount() {
        if (this.global.auth === false) {
            //window.location.replace('/login');
        }

    }

    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    handleProgress = progress => this.setState({ progress });

    handleUploadError = error => {
        this.setState({ isUploading: false });
    };

    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref("slider")
            .child(filename)
            .getDownloadURL()
            .then(url => {
                this.setState({avatarURL: url, filename: filename});
            }).then(() => {
            this.insertSlider();
        });

    };

    insertSlider() {
        firebase.firestore().collection('slider').add({
            img: this.state.avatarURL,
            filename: this.state.filename
        }).then(value => {
            toast.success('Immagine inserita');
        });
    }

    deleteSlider(key, filename) {
        firebase.firestore().collection('slider').doc(key).delete().then(() => {
            firebase.storage().ref('slider').child(filename).delete().then(() => {
                toast.success('Immagine eliminata');
            })
        });
    }




    render() {
        return (
            <div>
                <section id="single-page-slider" className="no-margin" style={{backgroundColor: '#7A7A7A'}}>
                    <div className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner">
                            <div className="item active">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="center gap fade-down section-heading">
                                                <h2 className="main-title">Gestisci Slider</h2>
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="center gap fade-down section-heading">
                                <h2 className="main-title">Aggiungi immagine</h2>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="tile-progress tile-cyan bounce-in">

                                <div className="center">

                                    <CustomUploadButton
                                        accept="image/*"
                                        name="avatar"
                                        storageRef={firebase.storage().ref("slider")}
                                        onUploadStart={this.handleUploadStart}
                                        onUploadError={this.handleUploadError}
                                        onUploadSuccess={this.handleUploadSuccess}
                                        onProgress={this.handleProgress}
                                        style={{backgroundColor: 'green', color: 'white', padding: 10, borderRadius: 4, marginTop: '2%', marginBottom: '2%'}}
                                    >
                                        Carica Foto
                                    </CustomUploadButton>

                                    {this.state.isUploading &&
                                    <div>
                                        <Line percent={this.state.progress} strokeWidth="2" strokeColor={this.state.color}/>
                                        <div className="tile-footer">
                                            <h4>
                                                <span className="pct-counter counter">{this.state.progress}</span>%
                                            </h4>
                                        </div>
                                    </div>

                                    }
                                </div>


                            </div>
                        </div>

                    </div>
                </div>

                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="center gap fade-down section-heading">
                                <h2 className="main-title">rimuovi immagine</h2>
                            </div>
                        </div>

                    </div>
                </div>

                <div style={{marginTop: '5%'}}>
                    <FirestoreCollection
                        path="slider"
                        render={({ isLoading, data }) => {
                            return isLoading ? (
                                <div className="col-md-12 text-center">
                                    <Loader
                                        type="Puff"
                                        color="black"
                                        height="100"
                                        width="100"
                                    />
                                </div>
                            ) : (
                                <div className="container">

                                    <div className="row">
                                        <ul className="portfolio-items col-3 isotope fade-up">
                                            {
                                                data.map(image => (
                                                    <li className="portfolio-item apps isotope-item"key={image.id}>
                                                        <div className="item-inner center">
                                                            <img src={image.img} alt="" className="img-responsive"/>
                                                            <a onClick={() => {
                                                                this.deleteSlider(image.id, image.filename)

                                                            }} href="#"
                                                               className="btn btn-outlined btn-danger" style={{margin: '2%'}}>Rimuovi</a>
                                                        </div>
                                                    </li>
                                                ))}
                                        </ul>

                                    </div>

                                </div>
                            );
                        }}
                    />
                </div>








                <ToastContainer/>
            </div>

        );
    }
}

export default ManageSlider;